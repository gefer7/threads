package br.com;

public class ThreadPrincipal {

    public static void main(String[] args) {

        new ThreadPrincipal().executaThreads();
    }

    public void executaThreads() {
        Thread1 t1 = new Thread1();
        new Thread(t1).start();

        Thread2 t2 = new Thread2();
        new Thread(t2).start();
    }

    class Thread1 implements Runnable {

        @Override
        public void run() {

            for (int i = 0; i < 500000; i++) {
                System.out.println(i);
            }
        }

    }

    class Thread2 implements Runnable {

        @Override
        public void run() {
            for (int i = 1000000; i < 1500000; i++) {
                System.out.println(i);
            }
        }
    }

}
